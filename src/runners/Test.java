package runners; /**
 * Created by Andrey on 21.04.2015.
 */
import static java.lang.Math.*;
public class Test {
    public static void main(String[] args) {
        double[] eps = {0.1, 0.3};
        double[] delta = {0.5, 0.33, 0.25, 0.1, 0.05, 0.03, 0.01 ,0.005, 0.0005};
        for (double ep : eps) {
            for (double v : delta) {
                System.out.println("delta:     " + v + "      eps:       " + ep + "      assimptotic:      " + assimptotic3(ep, v) + "        real:       " + real(ep, v) );
            }
            System.out.println();
        }
    }
    public static double assimptotic( double eps, double delta){
        return ( -0.5*( log(0.5*(1.-eps))/log(2.) + delta* (2.*log(eps/2.)/log(2.) + (2.*eps +1.)/(1.-eps) -2.*log((1-eps)/2.)) +
                delta*delta *((4.*eps + 2.)/eps - log(eps/2.)/log(2.) - (eps*eps - 0.5*eps)/((1.-eps)*(1.-eps))+ log((1.-eps)/2.)/log(2.) -
                        (4.*eps + 2.)/(1.-eps) )));
    }

    public static double assimptotic2( double eps, double delta){
        return -(eps * log(eps/2.)/log(2.) + (1. - eps) * log((1.-eps)/2.)/log(2.)) -
                delta * ((2.*eps -1.) * log ((1.-eps)/2.)/log(2.) + (1.-eps)*(log(eps/2.)/log(2.) + 1.) - eps * log(eps)/log(2.)) -
                delta* delta * ( eps/2. * log(eps) / log (2.) - eps * log ( (1.-eps)/2.) / log (2.) + (2.*(eps - 0.5)*(eps-0.5))/(1.-eps) + (eps - 2.)*(log(eps/2.)/log(2.) + 1.)*((1.-eps)*(1.-eps)/eps));
    }

    public static double assimptotic3( double eps, double delta){
        return -(2.* (0.5 * eps * log(0.5*eps)/log(2.) + ((0.5-eps)/log(2.) + (0.5 - eps)* log(0.5 * eps)/log(2.)) * delta  ) +
        2.* (0.5*(1.-eps) * log(0.5 * (1.-eps)) / log (2.) + (0.5 * eps / log(2.) - (eps- 0.5) * log(0.5*(1.-eps)) /log(2.) ) * delta));
    }


    public static double real (double eps, double delta ){
        double res = 0.;
        double p11 = 0.5 * eps * ( 1. - delta)*( 1. - delta);
        double p10 = 0.5 * (1. - eps) * ( 1. - delta)*( 1. - delta) + 0.5 * delta * ( 1. - delta);
        double p01 = p10;
        double p00 = 0.5 * eps * ( 1. - delta)*( 1. - delta) +  delta * ( 1. - delta) + delta*delta;
        res = nLogN(p11) + nLogN(p00) + nLogN(p10) + nLogN(p01);
        return res;

    }

    public static double nLogN(double x) {
        if (x == 0.)
            return 0;
        return -x * Math.log(x) / Math.log(2);
    }


}
