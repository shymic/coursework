package runners;

import worked.Counter;
import worked.Initializator;

/**
 * Created by Andrey on 28.09.2015.
 */
public class Runner {
    private static int L = 5;

    public static void main(String[] args) {

        Counter counter = new Counter(L);
        for (int i = L; i > 0; --i) {
            System.out.println(counter.showEmpiricEntropy(i));
            counter.setDecimal(counter.bitShift(counter.getDecimal()));
        }
    }
}
